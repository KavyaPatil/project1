
const problem3 = (arr) => {

    if(arr===undefined||arr.length===0||!Array.isArray(arr)){
        return [];
        }
    
    let temp;

    for(let index=0;index<arr.length;index++){
        for(let index2=index+1;index2<arr.length;index2++){

          if(arr[index].car_model.toUpperCase()>arr[index2].car_model.toUpperCase()){
            temp = arr[index];
            arr[index]=arr[index2];
            arr[index2]=temp;
          }
        }
      }

      return arr;
}

module.exports = problem3;