
const problem1 = (arr,iD) => {

if(arr===undefined||arr.length===0||!Array.isArray(arr)){
return [];
}

for(let index=0;index<arr.length;index++){ 
    if(arr[index].id===iD){
        //console.log(arr[index])
        console.log(`Car ${iD} is a ${arr[index].car_year} ${arr[index].car_make} ${arr[index].car_model}`)
        return arr[index];
    } 
}

return [];
}


module.exports = problem1;