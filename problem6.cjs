
const problem6 = (arr) => {

    if(arr===undefined||arr.length===0||!Array.isArray(arr)){
        return [];
        }

    const bmwAndAudiCars = [];

    for(let index=0;index<arr.length;index++){
        
        if(arr[index].car_make==='Audi' || arr[index].car_make==='BMW'){
            bmwAndAudiCars.push(arr[index]);
        }
    }

    return bmwAndAudiCars;
}

module.exports = problem6;