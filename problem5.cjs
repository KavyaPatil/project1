
const problem5 = (arr,year) => {

   if(arr===undefined||arr.length===0||!Array.isArray(arr)||!Number.isInteger(year)){
      return [];
      }
   
let olderCars=[];

for(let index=0;index<arr.length;index++){

   if(arr[index]<year){
    olderCars.push(arr[index]);
   }
}
return olderCars;
}

module.exports = problem5;

